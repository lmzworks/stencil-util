package works.lmz.stencil.util

import net.stickycode.stereotype.Configured
import works.lmz.common.stereotypes.SingletonBean

/**
 * Author: Marnix
 *
 * Configuration for util elements
 */
@SingletonBean
class UtilConfig {

    /**
     * Default faculty URL
     */
    private static final String DEFAULT_THEMES_INFO_URL = "http://cdn.works.lmz/style/themes.json"

    /**
     * Organisation themes URL is used to store the location of the themes information
     */
    @Configured
    String organisationThemesUrl = DEFAULT_THEMES_INFO_URL

}
